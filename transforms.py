from images import size, create_blank, read_img, write_img

def mirror(image: dict) -> dict:
    width, height = size(image)
    empty_image = create_blank(width, height)

    fila_arribaabajo = []
    for numero_fila in range(0, height, 1):
        start = numero_fila * width
        end = start + width
        row = image['pixels'][start:end]
        fila_arribaabajo.append(row)

    index = 0
    for numero_fila_reves in range(height-1, -1, -1): #(start, stop,step) al alto le restamos 1 porque los indices empiezan en 0, pero la altura es height
        start = numero_fila_reves * width
        end = start + width
        empty_image['pixels'][start:end] = fila_arribaabajo[index]
        index = index + 1

    return empty_image

img = read_img('cafe.jpg')
img_mirrored = mirror(img)
write_img(image=img_mirrored, filename="out.jpg")



